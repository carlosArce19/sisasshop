
$(document).ready(function () {
    initApp();
});

var initApp = function (e) {
    $("select[name='categoria']").change(function () {
        var datos = {"id": $("select[name='categoria']").val()};
        console.log(datos);
        $.ajax({
            url: 'http://127.0.0.1/sisa_Shop/app/panel/compruebaDatos',
            type: 'POST',
            data: datos,
            success: function (data) {
                $("select[name='subcategoria']").remove();
                $("#contenido").remove();

                var select = $(data);

                select.on('change', function () {
                    $("#contenido").fadeIn();
                });

                $("#cambiar").append(select);
                $("select[name='subcategoria']").fadeIn();

            },
            error: function () {
                alert('Error!');
            }
        });
// final ajax
    });

    $("select[name='productoEditar']").change(function () {
        var datos = {"id_producto": $("select[name='productoEditar']").val()};
        
        $.ajax({
            url: 'http://127.0.0.1/sisa_Shop/app/panel/traerDatosProducto',
            type: 'POST',
            data: datos,
            success: function (data) {
                $("select[name='imagenes']").remove();
                $("#contenidoProducto").remove();

                var select = $(data);

               /* select.on('change', function () {
                    $("#contenidoProducto").fadeIn();
                });*/

                $("#cambiarProducto").append(select);
                $("select[name='imagenes']").fadeIn();
                $("#contenidoProducto").fadeIn();

            },
            error: function () {
                alert('Error!');
            }
        });
// final ajax
    });

};






