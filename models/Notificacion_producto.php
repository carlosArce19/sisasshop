<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Notificacion_producto extends Model{
    //put your code here
    protected static $table="notificacion_producto";
    private $id;
    private $id_usuario;
    private $id_producto;
    
    
    function __construct($id,$id_usuario, $id_producto) {
        $this->id = $id;
        $this->id_usuario=$id_usuario;
        $this->id_producto = $id_producto;;
    }
      public function getMyVars() {
        return get_object_vars($this);
    }
    
    function getId() {
        return $this->id;
    }

    function getId_usuario() {
        return $this->id_usuario;
    }

    function getId_producto() {
        return $this->id_producto;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    function setId_producto($id_producto) {
        $this->id_producto = $id_producto;
    }



}