<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Notificacion_categoria extends Model{
    //put your code here
    protected static $table="notificacion_categoria";
    private $id;
    private $id_usuario;
    private $id_categoria;
    
    
    function __construct($id,$id_usuario, $id_categoria) {
        $this->id = $id;
        $this->id_usuario=$id_usuario;
        $this->id_categoria = $id_categoria;;
    }
    
    public function getMyVars() {
        return get_object_vars($this);
    }
    function getId() {
        return $this->id;
    }

    function getId_usuario() {
        return $this->id_usuario;
    }

    function getId_categoria() {
        return $this->id_categoria;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    function setId_categoria($id_categoria) {
        $this->id_categoria = $id_categoria;
    }


}

