<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Producto
 *
 * @author Usuario
 */
class Producto extends Model{
    protected static $table="producto";
    private $id;
    private $nombre;
    private $id_categoria;
    private $titulo;
    private $descripcion;
    private $precioVenta;
    private $precioCompra;
    private $codigo;
    private $codigoBarras;
    private $cantidad;
    private $fechaVencimiento;
    private $keywords;
    private $talla;
    private $color;
    private $editorial;
    private $fabricante;
    private $autor;
    private $peso;

    
      private $has_one = array(
        'categoria' => array(
            'class' => 'Categoria',
            'join_as' => 'id_categoria',
            'join_with' => 'id'
        )
    );
      
    private $has_many = array(
      'productos_x_bodega' => array(
       'class' => 'Bodega',
       'my_key' => 'id',
       'other_key' =>'id',
       'join_as' => 'id_producto',
       'join_with' => 'id_bodega',
       'data' => array(
       'valor'=> 'cantidad'    
       ),
       'join_table' => 'productos_x_bodega'
          
      ), 'carrito' => array(
       'class' => 'Usuario',
       'my_key' => 'id',
       'other_key' =>'id',
       'join_as' => 'id_producto',
       'join_with' => 'id_usuario',
       'data' => array(
       'valor'=> 'cantidad'    
       ),
       'join_table' => 'carrito'
          
      )
        
    );
      
    function __construct($id, $nombre, $id_categoria, $titulo, $descripcion, $precioVenta, $precioCompra, $codigo, $codigoBarras, $cantidad, $fechaVencimiento, $keywords, $talla, $color, $editorial, $fabricante, $autor, $peso) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->id_categoria = $id_categoria;
        $this->titulo = $titulo;
        $this->descripcion = $descripcion;
        $this->precioVenta = $precioVenta;
        $this->precioCompra = $precioCompra;
        $this->codigo = $codigo;
        $this->codigoBarras = $codigoBarras;
        $this->cantidad = $cantidad;
        $this->fechaVencimiento = $fechaVencimiento;
        $this->keywords = $keywords;
        $this->talla = $talla;
        $this->color = $color;
        $this->editorial = $editorial;
        $this->fabricante = $fabricante;
        $this->autor = $autor;
        $this->peso = $peso;
    }

        public function getMyVars() {
        return get_object_vars($this);
    }
    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getId_categoria() {
        return $this->id_categoria;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setId_categoria($id_categoria) {
        $this->id_categoria = $id_categoria;
    }
    
    public function getTitulo() {
        return $this->titulo;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getImagenes() {
        return $this->imagenes;
    }

    public function getPrecioVenta() {
        return $this->precioVenta;
    }

    public function getPrecioCompra() {
        return $this->precioCompra;
    }

    public function getCodigo() {
        return $this->codigo;
    }

    public function getCodigoBarras() {
        return $this->codigoBarras;
    }

    public function getCantidad() {
        return $this->cantidad;
    }

    public function getFechaVencimiento() {
        return $this->fechaVencimiento;
    }

    public function getKeywords() {
        return $this->keywords;
    }

    public function getTalla() {
        return $this->talla;
    }

    public function getColor() {
        return $this->color;
    }

    

    public function getEditorial() {
        return $this->editorial;
    }

    public function getFabricante() {
        return $this->fabricante;
    }

    public function getAutor() {
        return $this->autor;
    }

    public function getPeso() {
        return $this->peso;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setImagenes($imagenes) {
        $this->imagenes = $imagenes;
    }

    public function setPrecioVenta($precioVenta) {
        $this->precioVenta = $precioVenta;
    }

    public function setPrecioCompra($precioCompra) {
        $this->precioCompra = $precioCompra;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function setCodigoBarras($codigoBarras) {
        $this->codigoBarras = $codigoBarras;
    }

    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    public function setFechaVencimiento($fechaVencimiento) {
        $this->fechaVencimiento = $fechaVencimiento;
    }

    public function setKeywords($keywords) {
        $this->keywords = $keywords;
    }

    public function setTalla($talla) {
        $this->talla = $talla;
    }

    public function setColor($color) {
        $this->color = $color;
    }


    public function setEditorial($editorial) {
        $this->editorial = $editorial;
    }

    public function setFabricante($fabricante) {
        $this->fabricante = $fabricante;
    }

    public function setAutor($autor) {
        $this->autor = $autor;
    }

    public function setPeso($peso) {
        $this->peso = $peso;
    }

        
    public function getHas_one() {
        return $this->has_one;
    }

    public function setHas_one($has_one) {
        $this->has_one = $has_one;
    }
    
    public function getHas_many() {
        return $this->has_many;
    }

    public function setHas_many($has_many) {
        $this->has_many = $has_many;
    }




}
