-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-03-2016 a las 03:09:20
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sisashop`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bodega`
--

CREATE TABLE `bodega` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `id_tienda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bodega`
--

INSERT INTO `bodega` (`id`, `nombre`, `id_tienda`) VALUES
(1, 'bodega pokemon', 1),
(2, 'segunda pokemon', 1),
(3, 'aja', 1),
(4, 'la fortuna', 1),
(5, 'La Pura Bodega', 2),
(6, 'La Pura Bodega 2', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `id_usuario` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `id_padre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`, `id_padre`) VALUES
(47, 'zapatos', NULL),
(48, 'guayos', 47),
(49, 'Libros', NULL),
(50, 'Fantasía', 49),
(51, 'Alimentos', NULL),
(52, 'Muebles', NULL),
(53, 'Salmon', 51),
(54, 'Carnes', 51),
(55, 'Poltronas', 52);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenido`
--

CREATE TABLE `contenido` (
  `id` int(11) NOT NULL,
  `contenido` varchar(250) NOT NULL,
  `id_categoria` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contenido`
--

INSERT INTO `contenido` (`id`, `contenido`, `id_categoria`) VALUES
(8, 'nombreProducto,descripcion,imagenes,talla,color,fabricante,', 47),
(9, 'nombreProducto,titulo,descripcion,imagenes,editorial,autor,', 49),
(10, 'nombreProducto,descripcion,imagenes,fechaVencimiento,keywords,fabricante,peso,', 51),
(11, 'nombreProducto,descripcion,imagenes,keywords,color,fabricante,', 52);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen`
--

CREATE TABLE `imagen` (
  `id` int(11) NOT NULL,
  `url` varchar(250) NOT NULL,
  `id_producto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `imagen`
--

INSERT INTO `imagen` (`id`, `url`, `id_producto`) VALUES
(1, 'http://127.0.0.1/sisa_Shop/app/public/productos/la llegada de los tres.jpg', 67),
(2, 'http://127.0.0.1/sisa_Shop/app/public/productos/la-llegada-de-los-tres-la-torre-oscura-ii-9788499083834.jpg', 67),
(5, 'http://127.0.0.1/sisa_Shop/app/public/productos/tierras-baldias.jpg', 69),
(12, 'http://127.0.0.1/sisa_Shop/app/public/productos/27.jpg', 76),
(13, 'http://127.0.0.1/sisa_Shop/app/public/productos/bola-de-lomo.png', 76),
(14, 'http://127.0.0.1/sisa_Shop/app/public/productos/12609406-Pareja-de-reci-n-nacidos-amarillas-pollitos-de-pascua-vestida-de-novia-y-el-novio-para-una-boda-Foto-de-archivo.jpg', 69),
(15, 'http://127.0.0.1/sisa_Shop/app/public/productos/mueble+de+sala+3+2+1 tapizados.jpg', 77),
(16, 'http://127.0.0.1/sisa_Shop/app/public/productos/Mueble4.png', 77);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificacion`
--

CREATE TABLE `notificacion` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `tipo_elemento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificacion_categoria`
--

CREATE TABLE `notificacion_categoria` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `notificacion_categoria`
--

INSERT INTO `notificacion_categoria` (`id`, `id_usuario`, `id_categoria`) VALUES
(1, 7, 47),
(2, 7, 49),
(3, 7, 50),
(4, 7, 48),
(5, 7, 54),
(6, 7, 51),
(7, 7, 53),
(8, 7, 52);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificacion_producto`
--

CREATE TABLE `notificacion_producto` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `notificacion_producto`
--

INSERT INTO `notificacion_producto` (`id`, `id_usuario`, `id_producto`) VALUES
(1, 7, 67),
(3, 7, 65),
(5, 7, 69);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `titulo` varchar(150) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `precioVenta` int(11) NOT NULL,
  `precioCompra` int(11) NOT NULL,
  `codigo` int(11) NOT NULL,
  `codigoBarras` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fechaVencimiento` date DEFAULT NULL,
  `keywords` varchar(250) DEFAULT NULL,
  `talla` varchar(75) DEFAULT NULL,
  `color` varchar(250) DEFAULT NULL,
  `editorial` varchar(250) DEFAULT NULL,
  `fabricante` varchar(250) DEFAULT NULL,
  `autor` varchar(250) DEFAULT NULL,
  `peso` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `nombre`, `id_categoria`, `titulo`, `descripcion`, `precioVenta`, `precioCompra`, `codigo`, `codigoBarras`, `cantidad`, `fechaVencimiento`, `keywords`, `talla`, `color`, `editorial`, `fabricante`, `autor`, `peso`) VALUES
(65, 'no joda', 48, 'no jodas eche', '', 800, 20000, 777, 0, 0, NULL, NULL, '25', 'verde', NULL, 'puma', NULL, NULL),
(67, 'La Torre Oscura 2', 50, 'La llegada de los tres', 'rola continua su viaje despues de enfrentar a walter', 35000, 45000, 1234, 12345, 100, NULL, NULL, NULL, NULL, 'La Trama', NULL, 'Stephen King', NULL),
(69, 'papaya', 50, 'fruta rica', 'que rico esto', 4545, 21212, 1454, 53422, 124, NULL, NULL, NULL, NULL, 'dfdf', NULL, 'eeretw', NULL),
(76, 'Bola Nigga', 54, NULL, 'carne bola negra de lada', 7000, 5000, 8962, 741256, 200, '2016-04-30', 'carne,rica,blanda', NULL, NULL, NULL, 'rica rondo', NULL, '2'),
(77, 'Sillon Familiar', 55, NULL, 'cómodo sillon', 200000, 180000, 56933, 896523, 200, NULL, 'silla,mueble,comoda', NULL, 'cafe', NULL, 'un viejito', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_x_bodega`
--

CREATE TABLE `productos_x_bodega` (
  `id_bodega` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos_x_bodega`
--

INSERT INTO `productos_x_bodega` (`id_bodega`, `id_producto`, `cantidad`) VALUES
(1, 65, 0),
(5, 67, 100),
(5, 69, 444),
(5, 76, 199),
(5, 77, 200),
(6, 65, 50);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`) VALUES
(1, 'Administrador'),
(4, 'Cliente'),
(5, 'Cliente Fiel'),
(6, 'Cliente Mayorista'),
(3, 'Proveedor'),
(2, 'Vendedor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `template`
--

CREATE TABLE `template` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `uri` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `template`
--

INSERT INTO `template` (`id`, `nombre`, `preview`, `uri`) VALUES
(5, 'arce', NULL, 'http://127.0.0.1/sisa_Shop/app/views/templates/arce'),
(6, 'juansito', NULL, 'http://127.0.0.1/sisa_Shop/app/views/templates/juansito');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tienda`
--

CREATE TABLE `tienda` (
  `id` int(11) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `owner` int(11) NOT NULL,
  `id_template` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tienda`
--

INSERT INTO `tienda` (`id`, `titulo`, `owner`, `id_template`) VALUES
(1, 'Pokemon', 5, 5),
(2, 'La Pura', 6, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_notificacion`
--

CREATE TABLE `tipo_notificacion` (
  `id` int(11) NOT NULL,
  `tipo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_notificacion`
--

INSERT INTO `tipo_notificacion` (`id`, `tipo`) VALUES
(1, 'nuevos'),
(2, 'existencia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `rol` int(11) NOT NULL DEFAULT '4'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `apellido`, `password`, `email`, `rol`) VALUES
(5, 'Juan', 'Betancourt', '231293k', 'juan931223@hotmail.com', 1),
(6, 'Carlos', 'Arce Acosta', '123', 'k-rlos9403@hotmail.com', 1),
(7, 'Carlos', 'Arce', '123', 'carlosarce16@hotmail.com', 4),
(8, 'juan', 'betancourt', '123', 'jay@gmail.com', 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bodega`
--
ALTER TABLE `bodega`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bodega_tienda1_idx` (`id_tienda`);

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD PRIMARY KEY (`id_usuario`,`id_producto`),
  ADD KEY `fk_carrito_producto1_idx` (`id_producto`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_categoria_categoria1_idx` (`id_padre`);

--
-- Indices de la tabla `contenido`
--
ALTER TABLE `contenido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_categoria`);

--
-- Indices de la tabla `imagen`
--
ALTER TABLE `imagen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_producto` (`id_producto`);

--
-- Indices de la tabla `notificacion`
--
ALTER TABLE `notificacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_notificacion_tipo_notificacion1_idx` (`tipo_elemento`),
  ADD KEY `fk_notificacion_usuario1_idx` (`id_usuario`);

--
-- Indices de la tabla `notificacion_categoria`
--
ALTER TABLE `notificacion_categoria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_categoria` (`id_categoria`);

--
-- Indices de la tabla `notificacion_producto`
--
ALTER TABLE `notificacion_producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_producto` (`id_producto`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_producto_categoria` (`id_categoria`);

--
-- Indices de la tabla `productos_x_bodega`
--
ALTER TABLE `productos_x_bodega`
  ADD PRIMARY KEY (`id_bodega`,`id_producto`),
  ADD KEY `fk_productos_x_bodega_producto1_idx` (`id_producto`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre_UNIQUE` (`nombre`);

--
-- Indices de la tabla `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tienda`
--
ALTER TABLE `tienda`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tienda_template2_idx` (`id_template`),
  ADD KEY `fk_tienda_usuario2_idx` (`owner`);

--
-- Indices de la tabla `tipo_notificacion`
--
ALTER TABLE `tipo_notificacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_usuario_roles_idx` (`rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bodega`
--
ALTER TABLE `bodega`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT de la tabla `contenido`
--
ALTER TABLE `contenido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `imagen`
--
ALTER TABLE `imagen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `notificacion`
--
ALTER TABLE `notificacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `notificacion_categoria`
--
ALTER TABLE `notificacion_categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `notificacion_producto`
--
ALTER TABLE `notificacion_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `template`
--
ALTER TABLE `template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tienda`
--
ALTER TABLE `tienda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tipo_notificacion`
--
ALTER TABLE `tipo_notificacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `bodega`
--
ALTER TABLE `bodega`
  ADD CONSTRAINT `fk_bodega_tienda1` FOREIGN KEY (`id_tienda`) REFERENCES `tienda` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD CONSTRAINT `fk_carrito_producto1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_carrito_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD CONSTRAINT `fk_categoria_categoria1` FOREIGN KEY (`id_padre`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `contenido`
--
ALTER TABLE `contenido`
  ADD CONSTRAINT `contenido_fk` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `imagen`
--
ALTER TABLE `imagen`
  ADD CONSTRAINT `imagen_fk` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `notificacion`
--
ALTER TABLE `notificacion`
  ADD CONSTRAINT `fk_notificacion_tipo_notificacion1` FOREIGN KEY (`tipo_elemento`) REFERENCES `tipo_notificacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_notificacion_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `notificacion_categoria`
--
ALTER TABLE `notificacion_categoria`
  ADD CONSTRAINT `fk_notificacion_categoria_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_notificacion_categoria_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `notificacion_producto`
--
ALTER TABLE `notificacion_producto`
  ADD CONSTRAINT `fk_notificacion_producto_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_notificacion_producto_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos_x_bodega`
--
ALTER TABLE `productos_x_bodega`
  ADD CONSTRAINT `fk_productos_x_bodega_bodega1` FOREIGN KEY (`id_bodega`) REFERENCES `bodega` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_productos_x_bodega_producto1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tienda`
--
ALTER TABLE `tienda`
  ADD CONSTRAINT `fk_tienda_template2` FOREIGN KEY (`id_template`) REFERENCES `template` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tienda_usuario2` FOREIGN KEY (`owner`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_roles` FOREIGN KEY (`rol`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
