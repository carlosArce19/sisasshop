<?php

class Categoria extends Model{
  protected static $table="categoria";
  private $id;
  private $nombre;
  private $id_padre;
  
      private $has_one = array(
        'categoria' => array(
            'class' => 'Categoria',
            'join_as' => 'id_padre',
            'join_with' => 'id'
        )
    );
      
  
    function __construct($id, $nombre, $id_padre) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->id_padre = $id_padre;
    }
    
    
    public function getMyVars() {
        return get_object_vars($this);
    }
    
    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getId_padre() {
        return $this->id_padre;
    }

    public function getHas_one() {
        return $this->has_one;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setId_padre($id_padre) {
        $this->id_padre = $id_padre;
    }

    public function setHas_one($has_one) {
        $this->has_one = $has_one;
    }


}
