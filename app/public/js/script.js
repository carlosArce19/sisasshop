$(document).ready(function() {
	$(function(){
			$("#menuDesplegableBoton").click(function(){
				$("header nav").slideToggle();
			});
			$(window).resize(function(){
				menu();
				if(($("#tituloIntroduccion").text()) == "Bienvenido" ) slide();
			});

			menu();

			if(($("#tituloIntroduccion").text()) == "Portafolio" ) galeria();

			function menu(){
				if($(window).width()>633){
					$("header nav").css({"display":"block"});
				}else{
					$("header nav").css({"display":"none"});
				}
			}

			if(($("#tituloIntroduccion").text()) == "Bienvenido" ) slide();

			function slide(){
				
				if( $(window).width() > 900 ){
					$('#slides').slidesjs({

					    width: 940,
					    height: 608,
					    navigation: false
			    		    	
		      		});
		      	} else if( $(window).width() > 650 && $(window).width() < 900 ){
		      		$('#slides').slidesjs({

					    width: 940,
					    height: 660,
					    navigation: false 		    	
		      		});
		      	} else{

		      		$('#slides').slidesjs({

					    width: 940,
					    height: 660,
					    navigation: false 		    	
		      		});
		      	}
			}

			var cancionMuse = document.createElement("audio");
			cancionMuse.src = "./assets/Feelinggood.mp3";
			document.body.appendChild(cancionMuse);
			if(($("#tituloIntroduccion").text()) == "Bienvenido" ) cancionMuse.play();
			
			$(".play").click(function(){

				reproductor();
			})

			function galeria(){	

				$('.gallery').featherlightGallery({
					gallery: {
						fadeIn: 300,
						fadeOut: 300
					},
					openSpeed:    300,
					closeSpeed:   300
				});
				$('.gallery2').featherlightGallery({
					gallery: {
						next: 'next »',
						previous: '« previous'
					},
					variant: 'featherlight-gallery2'
				});
			}

			function reproductor(){

				if(cancionMuse.paused){
					cancionMuse.play();
				}else if(!cancionMuse.paused){
					console.log("Si");
					cancionMuse.pause();
				}
			}

	});
});