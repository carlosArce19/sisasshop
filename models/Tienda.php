<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tienda
 *
 * @author Usuario
 */
class Tienda extends Model {

    protected static $table = "tienda";
    private $id;
    private $titulo;
    private $owner;
    private $id_template;

    function __construct($id, $titulo, $owner, $id_template) {
        $this->id = $id;
        $this->titulo = $titulo;
        $this->owner = $owner;
        $this->id_template = $id_template;
    }
    
    public function updateTemplate($id_template) {
        parent::update();
    }

    public function getMyVars() {
        return get_object_vars($this);
    }

    public function getId() {
        return $this->id;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getOwner() {
        return $this->owner;
    }

    public function getId_template() {
        return $this->id_template;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setOwner($owner) {
        $this->owner = $owner;
    }

    public function setId_template($id_template) {
        $this->id_template = $id_template;
    }

}
