<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Test_controller
 *
 * @author pabhoz
 */
class Test_controller extends Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
       
        /*$usr = new Usuario(null,"Cindy","Triviño",0,"15",null);
        $r = $usr->create();
        print_r($r);
    $cindy = Usuario::getById(1);
        $jr = Usuario::getBy("nombre", "Jr");
        $cindy->has_one("valentin",$jr);
        $cindy->update();
          */
        print_r (get_class($this));
        $this->view->render($this,"juansito","index");
        
    }
    
    public function readJSON($src){
        $r = json_decode(file_get_contents('./public/'.$src.'.js'), true);
        print_r($r);
    }
    
    public function encriptar($str){
        echo Hash::create($str);
    }
    
    public function ende($str){
        $key = "daniel";
        $ida = Hash::encrypt($str, $key);
        $vuelta = Hash::decrypt($ida, $key);
        
        echo "Ida: ".$ida." Vuelta: ".$vuelta;
        Logger::msg("vuelta");
    }
    
    public function amigar(){
        $daniel = new Usuario(null, "lola", "1234");
        print_r($daniel->create());
        $salazar = new Usuario(null, "pepe", "prima");
        print_r($salazar->create());
        $daniel->has_many("amigos", $salazar);
        print_r($daniel->update());
    }
    
    public function snap(){
        
        Snaps_BL::enviarSnap(Usuario::getBy("username", $_GET["from"]),
        Usuario::getBy("username",$_GET["to"]), null);
    }
    
}
