<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Contenido
 *
 * @author Usuario
 */
class Contenido extends Model {
    protected static $table = "contenido";
    private $id;
    private $contenido;
    private $id_categoria;
    
     private $has_one = array(
     'contenido' => array(
     'class' => 'Categoria',
     'join_as' => 'id_categoria',
     'join_with' => 'id'
     )
    );
     
    function __construct($id, $contenido, $id_categoria) {
        $this->id = $id;
        $this->contenido = $contenido;
        $this->id_categoria = $id_categoria;
    }
    
       public function getMyVars() {
        return get_object_vars($this);
    }
    public function getId() {
        return $this->id;
    }


    public function getId_categoria() {
        return $this->id_categoria;
    }

    public function getHas_one() {
        return $this->has_one;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getContenido() {
        return $this->contenido;
    }

    public function setContenido($contenido) {
        $this->contenido = $contenido;
    }

    
    public function setId_categoria($id_categoria) {
        $this->id_categoria = $id_categoria;
    }

    public function setHas_one($has_one) {
        $this->has_one = $has_one;
    }

    
    //put your code here
}
