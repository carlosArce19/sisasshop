<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Bodega
 *
 * @author Usuario
 */
class Bodega extends Model {
    protected static $table="bodega";
    private $id;
    private $nombre;
    private $id_tienda;
    
    private $has_many = array(
      'productos_x_bodega' => array(
       'class' => 'Producto',
       'my_key' => 'id',
       'other_key' =>'id',
       'join_as' => 'id_bodega',
       'join_with' => 'id_producto',
       'data' => array(
       'valor'=> 'cantidad'    
       ),
       'join_table' => 'productos_x_bodega'
          
      ) 
        
    );
    
    private $has_one = array(
     'tienda' => array(
     'class' => 'Tienda',
     'join_as' => 'id_tienda',
     'join_with' => 'id'
     )
    );
    
    function __construct($id, $nombre, $id_tienda) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->id_tienda = $id_tienda;
    }
    
    
    
    public function getMyVars() {
        return get_object_vars($this);
    }

    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getId_tienda() {
        return $this->id_tienda;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setId_tienda($id_tienda) {
        $this->id_tienda = $id_tienda;
    }

    public function getHas_many() {
        return $this->has_many;
    }

    public function getHas_one() {
        return $this->has_one;
    }

    public function setHas_many($has_many) {
        $this->has_many = $has_many;
    }

    public function setHas_one($has_one) {
        $this->has_one = $has_one;
    }


}
