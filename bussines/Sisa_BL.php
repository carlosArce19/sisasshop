<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Snaps_BL
 *
 * @author pabhoz
 */
class Sisa_BL {

    public static function enviarSnap($sender, $adressee, $snap) {
        //verificar que sean amigos
        if ($sender->isMyfriend($adressee)) {
            echo "Podemos mandarnos pendejadas";
        } else {
            Logger::alert("No son amigos", null, "enviarSnap");
        }
    }

    public static function categoriaDatos($id) {
        if ($id != "0") {
            $cont = Contenido::search("id_categoria =" . $id);
            $subc = Categoria::search("id_padre =" . $id);
            echo '<select name="subcategoria" style="display:none">';
            echo '<option value="">Seleccione a que subcategoria pertenece</option>';
            foreach ($subc as $sub) {
                echo '<br>';
                echo '<option value="' . $sub['id'] . '">' . $sub['nombre'] . '</option>';
            }
            echo '</select>';
            $contenido = $cont[0]["contenido"];
            $contenido = explode(",", $contenido);

            $index = count($contenido);
            $index = $index - 1;
            $i = 0;

           
            echo '<div id="contenido" style ="display:none">';
            echo '<br>';
            echo 'Precio de venta: <input type="text" name="precioVenta"><br>';
            echo 'Precio de compra: <input type="text" name="precioCompra"><br>';
            echo 'Codigo: <input type="text" name="codigo"><br>';
            echo 'Codigo de barras: <input type="text" name="codigoBarras"><br>';
            echo 'Cantidad: <input type="text" name="cantidad"><br>';
            foreach ($contenido as $conte) {
                $i++;
                if ($i <= $index) {
                    if ($conte != "imagenes" && $conte != "fechaVencimiento") {
                        echo $conte . ' <input type="text" name="' . $conte . '">' . '<br>';
                    } elseif ($conte == "fechaVencimiento") {
                        echo $conte . '<input type="date" name="' . $conte . '">' . '<br>';
                    } elseif ($conte == "imagenes") {
                        echo $conte . ':  <input type="file" name="files[]" multiple>' . '<br>';
                    }        }
            }
            echo '<br>';
            echo '<input id="creando" type="submit" value="Crear Producto">';
            echo '</div>';
        }
        
    }

    public static function productosDatos($id) {
        if ($id != "0") {
            $imagenes = Imagen::search("id_producto=" . $id);
            $producto= Producto::getById($id);
            $subCat= Categoria::getById($producto->getId_categoria());
            $cont = Contenido::search("id_categoria =" . $subCat->getId_padre());
            echo '<select name="imagenes" style="display:none">';
            echo '<option value="-1">Seleccione imagen a eliminar</option>';
            foreach ($imagenes as $img) {
                echo '<option value="' . $img['id'] . '">' . $img['id'] . '</option>';
            }
            echo '</select>';
            echo '<br><br>';
            $contenido = $cont[0]["contenido"];
            $contenido = explode(",", $contenido);

            $index = count($contenido);
            $index = $index - 1;
            $i = 0;

            echo '<div id="contenidoProducto" style ="display:none">';
            echo 'Precio de venta: <input type="text" name="precioVenta" ><br>';
            echo 'Precio de compra: <input type="text" name="precioCompra" ><br>';
            echo 'Codigo: <input type="text" name="codigo" ><br>';
            echo 'Codigo de barras: <input type="text" name="codigoBarras" ><br>';
            echo 'Keyword: <input type="text" name="codigoBarras" ><br>';
           
            foreach ($contenido as $conte) {
                $i++;
                if ($i <= $index) {
                    if ($conte != "imagenes" && $conte != "fechaVencimiento") {
                        echo $conte . ': <input type="text" name="' . $conte . '">' . '<br>';
                    } elseif ($conte == "fechaVencimiento") {
                        echo $conte . ': <input type="date" name="' . $conte . '">' . '<br>';
                    } elseif ($conte == "imagenes") {
                        echo $conte . ': <input type="file" name="files[]" multiple>' . '<br>';
                    }        }
            }
            
            echo '<br>';
            echo '<input type="checkbox" name="eliminar" value="1"> Eliminar Producto<br>';
            echo '<input id="editar" type="submit" value="Editar Producto">';
            echo '</div>';
        }
    }

}
