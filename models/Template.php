<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Template
 *
 * @author Usuario
 */
class Template extends Model {

    //put your code here
    protected static $table = "Template";
    private $id;
    private $nombre;
    private $preview;
    private $uri;

    function __construct($id, $nombre, $preview, $uri) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->preview = $preview;
        $this->uri = $uri;
    }

    public function getMyVars() {
        return get_object_vars($this);
    }
    
    

    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getPreview() {
        return $this->preview;
    }

    public function getUri() {
        return $this->uri;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setPreview($preview) {
        $this->preview = $preview;
    }

    public function setUri($uri) {
        $this->uri = $uri;
    }

}
