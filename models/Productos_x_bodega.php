<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Productos_x_bodega
 *
 * @author Usuario
 */
class Productos_x_bodega extends Model{
    //put your code here
    protected static $table="productos_x_bodega";
    private $id_bodega;
    private $id_producto;
    private $cantidad;
    
    
    function __construct($id_bodega, $id_producto, $cantidad) {
        $this->id_bodega = $id_bodega;
        $this->id_producto = $id_producto;
        $this->cantidad = $cantidad;
    }

      public function getMyVars() {
        return get_object_vars($this);
    }
    
    public static function getTable() {
        return self::$table;
    }

    public function getId_bodega() {
        return $this->id_bodega;
    }

    public function getId_producto() {
        return $this->id_producto;
    }

    public function getCantidad() {
        return $this->cantidad;
    }

    public static function setTable($table) {
        self::$table = $table;
    }

    public function setId_bodega($id_bodega) {
        $this->id_bodega = $id_bodega;
    }

    public function setId_producto($id_producto) {
        $this->id_producto = $id_producto;
    }

    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }


}
