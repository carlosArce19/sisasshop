<?php

/**
 * Description of View
 *
 * @author pabhoz
 */
class View {

    function render($controller,$p,$view, $title = "") {
        $controller = get_class($controller);
        $controller = substr($controller, 0, -11);
        $path = './views/' . 'templates/'.$p.'/'.$controller . '/' . $view;
        

        if (file_exists($path . ".php")) {
            require $path . ".php";
        } elseif (file_exists($path . ".html")) {
            require $path . ".html";
        } else {
            echo "Error: Invalid view " . $view . " to render";
        }
    }
    

}
