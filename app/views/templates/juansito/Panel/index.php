<!DOCTYPE html>

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <title>Panel Admin Juansito</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script src="./libs/app.js"></script>
        <link rel="stylesheet" href="./views/templates/juansito/Panel/css/app.css">
        <link href="<?php echo _URL; ?>/public/assets/ionicons-2.0.1/css/ionicons.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div class="panelIzq"> 
            <div class="barraH">
                <div class="crearCategoria"><i class="ion-android-create"></i> Crear Categoria</div>
                <div class="crearSubcategoria"><i class="ion-android-create"></i>Crear Subcategoria</div>
                <div class="crearProducto"><i class="ion-android-create"></i> Crear Producto</div>
                <div class="subirTemplate"><i class="ion-android-archive"></i> Subir Template</div>
                <div class="cambiarTemplate"><i class="ion-compose"></i> Cambiar Template</div>
                <div class="crearBodega"><i class="ion-android-create"></i> Crear Bodega</div>
                <div class="aumentarStock"><i class="ion-compose"></i> Aumentar Stock</div>
                <div class="enviarCorreos"><i class="ion-android-mail"></i> enviarCorreos</div>
                <div class="editarProducto"><i class="ion-edit"></i> Editar Producto</div>
            </div>
        </div>
        <div class="workspace">

          <div id="crearCategoria" class="hidden area" >
            <div class="titulo"> CREAR CATEGORIA</div>
              <form  action="<?php echo _URL; ?>/panel/categoria" method="POST">
                  <br>Nombre categoria <input type="text" name="nombre"><br><br>
                  Que opciones tendra su categoria:<br><br>
                  <div class="check">
                  <input type="checkbox" name="nombreProducto" value="nombreProducto," /> Nombre Producto.
                   </div>
                  <div class="check">
                  <input type="checkbox" name="titulo" value="titulo," /> Titulo.
                   </div>
                  <div class="check">
                  <input type="checkbox" name="descripcion" value="descripcion," /> Descripcion.
                   </div>
                  <div class="check">
                  <input type="checkbox" name="imagenes" value="imagenes," /> Imagenes.<br />
                   </div>
                  <div class="check">
                  <input type="checkbox" name="fechaVencimiento" value="fechaVencimiento," /> Fecha de Vencimiento.<br />
                   </div>
                  <div class="check">
                  <input type="checkbox" name="keywords" value="keywords," /> Keywords.
                   </div>
                  <div class="check">
                  <input type="checkbox" name="talla" value="talla," /> Talla.
                   </div>
                  <div class="check">
                  <input type="checkbox" name="color" value="color," /> Color.<br />
                   </div>
                  <div class="check">
                  <input type="checkbox" name="editorial" value="editorial," /> Editorial.                                                                                                                                          
                   </div>
                  <div class="check">
                  <input type="checkbox" name="fabricante" value="fabricante," /> Fabricante.
                   </div>
                  <div class="check">
                  <input type="checkbox" name="autor" value="autor," /> Autor.<br />
                   </div>
                  <div class="check">
                  <input type="checkbox" name="peso" value="peso," /> Peso.<br />  
                   </div>
                  <div class="check">
                  <input type="submit" value="Crear Categoria">
                  </div>
              </form>
          </div>




            <div id="crearSubcategoria" class="hidden area" >
                <div class="titulo"> CREAR SUBCATEGORIA</div>
            <form  action="<?php echo _URL; ?>/panel/subcategoria" method="POST">
                <br><br><br>Nombre subcategoria: <input type="text" name="nombre"><br><br>
                <select name="id_padre">
                    <option value="">Seleccione Categoria</option>
                    <?php foreach ($this->categorias as $categoria) : ?>
                        <option value="<?php echo $categoria['id']; ?>"><?php echo $categoria['nombre']; ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="submit" value="Crear Subcategoria">
            </form>
            </div>    

            <div id="crearProducto" class="hidden area" >
                <div class="titulo"> CREAR PRODUCTO</div>
            <form  id="cambiar" action="<?php echo _URL; ?>/panel/crearProducto" method="POST" enctype="multipart/form-data">
                <br>
                <select name="categoria" style="display:block">
                    <option value="0">Seleccione a que categoria pertenece el producto</option>
                    <?php foreach ($this->categorias as $categoria) : ?>
                        <option value="<?php echo $categoria['id']; ?>"><?php echo $categoria['nombre']; ?></option>
                    <?php endforeach; ?>
                </select>
                <br>

            </form> 
            </div> 

            <div id="subirTemplate" class="hidden area" >
                <div class="titulo"> SUIR TEMPLATE</div>
            <form  action="<?php echo _URL; ?>/panel/template" method="POST" enctype="multipart/form-data">
               <br><br> Busque el Archivo .zip del Template:<br>
               <br><input type="file" name="file"><br>
                <br><input id="varios" class="button" type="submit" value="Subir Template">

            </form> 
            </div> 
            
            <div id="cambiarTemplate" class="hidden area" >
                <div class="titulo"> CAMBIAR TEMPLATE</div>
              <form action="<?php echo _URL; ?>/panel/cambiarTemplate" method="POST">
                  <br><br>Selecciones el Template:
                  <select name="id_template">
                      <option value="-1">Seleccione el template</option>
                      <?php foreach ($this->template as $tem) : ?>
                          <option value="<?php echo $tem['id']; ?>"><?php echo $tem['nombre']; ?></option>
                      <?php endforeach; ?>
                  </select><br><br>
                  Selecciones la tienda: 
                   <select  name="id_tienda">
                      <option value="-1">Seleccione la tienda</option>
                      <?php foreach ($this->tienda as $tien) : ?>
                          <option value="<?php echo $tien['id']; ?>"><?php echo $tien['titulo']; ?></option>
                      <?php endforeach; ?>
                  </select><br><br>
                  <input type="submit" value="Cambiar Template">
              </form> 
            </div>


            <div id="crearBodega" class="hidden area" >
                <div class="titulo"> CREAR BODEGA</div>
            <form  action="<?php echo _URL; ?>/panel/crearBodega" method="POST">
                <br><br>Nombre Bodega: <input type="text" name="nombre"><br><br>
                Tienda a la que pertenece: 
                <select name="id_tienda">
                    <option value="">Seleccione la tienda</option>
                    <?php foreach ($this->tienda as $tien) : ?>
                        <option value="<?php echo $tien['id']; ?>"><?php echo $tien['titulo']; ?></option>
                    <?php endforeach; ?>
                </select><br><br>
                <input type="submit" value="Crear Bodega">
            </form>  
            </div>     

            <div id="aumentarStock" class="hidden area" >
                <div class="titulo"> AUMENTAR STOCK</div>
               <form   action="<?php echo _URL; ?>/panel/añadirBodega" method="POST">
                   <br><br>Añadir productos a bodega<br>
                   <select name="id_producto">
                       <option value="">Seleccione el producto</option>
                       <?php foreach ($this->pro as $pro) : ?>
                           <option value="<?php echo $pro['id']; ?>"><?php echo $pro['nombre']; ?></option>
                       <?php endforeach; ?>
                   </select>
   
                   <select name="id_bodega">
                       <option value="">Seleccione la bodega</option>
                       <?php foreach ($this->bode as $bode) : ?>
                           <option value="<?php echo $bode['id']; ?>"><?php echo $bode['nombre']; ?></option>
                       <?php endforeach; ?>
                   </select>
                   <br><br>Cantidad: <input type="text" name="cantidad">
                   <br><br><input type="submit" value="Aumentar Stock">
               </form>
            </div> 

            <div id="enviarCorreos" class="hidden area" >
                <div class="titulo"> ENVIAR CORREOS</div>
               <form  name="enviarCorreos" action="<?php echo _URL; ?>/panel/enviarCorreos" method="POST">
                   <br><br>Asunto del Correo:
                   <input class="enviarCorreosValue" name="asunto" type="text" placeholder="asunto del correo" required/>
                   <br><br>
                   <textarea id="msgText"name="mensaje" rows="10" cols="50" placeholder="Mensaje"></textarea>
                   <br><br>
                   <select id="idTiendaSelcted2" name="id_tienda">
                       <option value="-1">Seleccione la tienda</option>
                       <?php foreach ($this->tienda as $tien) : ?>
                           <option value="<?php echo $tien['id']; ?>"><?php echo $tien['titulo']; ?></option>
                       <?php endforeach; ?>
                   </select>
                   <select id="idSelectedRol" name="id_rol">
                       <option value="-1">Seleccione a destinatarios</option>
                       <?php foreach ($this->rol as $rol) : ?>
                           <option value="<?php echo $rol['id']; ?>"><?php echo $rol['nombre']; ?></option>
                       <?php endforeach; ?>
                       <option value="-2">Todos</option>
                   </select>
                   <br><br>
                   <input id="vistPrevia" class="vistPreviaBtn" type="button" value="Vista Previa" onclick="vistaPrevia();"/>
                   <input class="enviarCorreosValue" id="enviarCorreosBtn" name="enviarCorreosBtn" type="submit" value="Enviar Correo" required/>
   
               </form>
            </div>
            
            <div id="editarProducto" class="hidden area" >
                <div class="titulo"> EDITAR PRODUCTO</div>
              <form id="cambiarProducto"  action="<?php echo _URL; ?>/panel/editarProducto" method="POST" enctype="multipart/form-data">
                  <br><br>Elija Producto a Editar<br><br>
                  <select name="productoEditar">
                      <option value="">Seleccione el producto</option>
                      <?php foreach ($this->pro as $pro) : ?>
                          <option value="<?php echo $pro['id']; ?>"><?php echo $pro['nombre']; ?></option>
                      <?php endforeach; ?>
                  </select>     
              </form> 
            </div>


        </div>
        <script>

            $(document).ready(function(){
                var actCrearCategoria=0;
                $(".crearCategoria").click(function(){

                   if (actCrearCategoria==0) {

                     hideAll();
                     $("#crearCategoria").show();
                     actCrearCategoria=1;
                   }else{

                    $("#crearCategoria").hide();
                     actCrearCategoria=0;
                   }
                });

                var actCrearSubcategoria=0;
                $(".crearSubcategoria").click(function(){

                   if (actCrearSubcategoria==0) {

                     hideAll();
                     $("#crearSubcategoria").show();
                     actCrearSubcategoria=1;
                   }else{

                    $("#crearSubcategoria").hide();
                    actCrearSubcategoria=0;
                   }
                });

                var actCrearProducto=0;
                $(".crearProducto").click(function(){

                    
                   if (actCrearProducto==0) {

                     hideAll();
                     $("#crearProducto").show();
                     actCrearProducto=1;
                   }else{

                    $("#crearProducto").hide();
                     actCrearProducto=0;
                   }
                });

                var actSubirTemplate=0;
                $(".subirTemplate").click(function(){

                   if (actSubirTemplate==0) {

                     hideAll();
                     $("#subirTemplate").show();
                     actSubirTemplate=1;
                   }else{

                    $("#subirTemplate").hide();
                     actSubirTemplate=0;
                   }
                });

                var actCambiarTemplate=0;
                $(".cambiarTemplate").click(function(){

                   if (actCambiarTemplate==0) {

                     hideAll();
                     $("#cambiarTemplate").show();
                     actCambiarTemplate=1;
                   }else{

                    $("#cambiarTemplate").hide();
                     actCambiarTemplate=0;
                   }
                });

                var actCrearBodega=0;
                $(".crearBodega").click(function(){

                   if (actCrearBodega==0) {

                     hideAll();
                     $("#crearBodega").show();
                     actCrearBodega=1;
                   }else{

                    $("#crearBodega").hide();
                     actCrearBodega=0;
                   }
                });

                var actAumentarStock=0;
                $(".aumentarStock").click(function(){

                   if (actAumentarStock==0) {

                     hideAll();
                     $("#aumentarStock").show();
                     actAumentarStock=1;
                   }else{

                    $("#aumentarStock").hide();
                     actAumentarStock=0;
                   }
                });

                var actEnviarCorreos=0;
                $(".enviarCorreos").click(function(){

                   if (actEnviarCorreos==0) {

                     hideAll();
                     $("#enviarCorreos").show();
                     actEnviarCorreos=1;
                   }else{

                    $("#enviarCorreos").hide();
                     actEnviarCorreos=0;
                   }
                });

                var actEditarProducto=0;
                $(".editarProducto").click(function(){

                   if (actEditarProducto==0) {

                    hideAll();
                     $("#editarProducto").show();
                     actEditarProducto=1;
                   }else{

                    $("#editarProducto").hide();
                     actEditarProducto=0;
                   }
                });
            });

            function hideAll(){

                $("#editarProducto").hide();
                actEditarProducto=0;

                $("#enviarCorreos").hide();
                actEnviarCorreos=0;

                $("#crearBodega").hide();
                actCrearBodega=0;

                $("#cambiarTemplate").hide();
                actCambiarTemplate=0;

                $("#subirTemplate").hide();
                actSubirTemplate=0;

                $("#crearProducto").hide();
                actCrearProducto=0;

                $("#crearSubcategoria").hide();
                actCrearSubcategoria=0;

                $("#crearCategoria").hide();
                actCrearCategoria=0;

                $("#aumentarStock").hide();
                actAumentarStock=0;

            }




            $(function () {

                $('#enviarCorreosBtn').click(function (e) {
                    e.preventDefault();
                    enviarCorreos();
                });
            });
            function vistaPrevia() {


                var tienda = $("#idTiendaSelcted2 option:selected").text();
                var mensaje = $("#msgText").val();
                alert("Buenas [Nombre Usuario] "+"de parte de " + tienda + "\n \n" + mensaje);
            }

            function enviarCorreos() {

                var asunto = $('form[name=enviarCorreos] input[name=asunto]')[0].value;
                var mensaje=$("#msgText").val();
                var tienda = $("#idTiendaSelcted2 option:selected").text();
                var destinatarios=$("#idSelectedRol").val();

                $.ajax({
                    type: "POST",
                    url: "<?php echo _URL; ?>/panel/enviarCorreos",
                    data: {asunto: asunto, mensaje: mensaje, destinatario:destinatarios,tienda:tienda}
                }).done(function (response) {

                    if (response == 1) {
                        alert("Mensaje Enviado Exitosamente");
                    } else {
                        alert(response);
                    }
                });
            }

        </script>

    </body>

</html>