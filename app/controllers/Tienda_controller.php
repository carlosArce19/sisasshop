<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tienda_controller
 *
 * @author Usuario
 */
class Tienda_controller extends Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $tienda=  Tienda::getById($_SESSION["TIENDA"]);
        $this->view->tien=$tienda;
        $template=  Template::getById($tienda->getId_template());
        $this->view->render($this, $template->getNombre(), "index");
    }

    public function traerDatos() {
        $cat = Categoria::search("id_padre is null");

        
        foreach ($cat as $categoria) {
            echo '<br>';
            echo '<div class="categoria">';
            echo $categoria["nombre"] ."<input class='suscribirCategoria' type='submit' categoria='" . $categoria["id"]. "' value='suscribir'>". "<br>";
            $sub = Categoria::search("id_padre =" . $categoria["id"]);
            
            foreach ($sub as $subcategoria) {
                echo '<div class="subcategoria">';
                echo "<a href='" . $subcategoria["id"] . "' class='producto'>";
                echo $subcategoria["nombre"] ."<input class='suscribirSubcategoria' type='submit' subcategoria='" . $subcategoria["id"] . "' value='suscribir'>"."<br>";
                echo "</a>";
                echo '</div>';
            }
            echo '</div>';
        }
    }

    public function traerProductos() {

        $pro = Producto::search("id_categoria = " . $_POST["id"]);
        $padre = Categoria::search("id =" . $_POST["id"]);
        $padre = Categoria::search("id =" . $padre[0]["id_padre"]);
        $cont = Contenido::search("id_categoria =" . $padre[0]["id"]);

        $contenido = $cont[0]["contenido"];
        $contenido = explode(",", $contenido);

        $index = count($contenido);
        $index = $index - 1;
        $i = 0;
        $j = 0;
        $k = 0;
        $n = -1;
   
          foreach ($pro as $producto) {
          echo '<div class="containerProducto">';
          $imagenes = Imagen::search("id_producto =".$producto["id"]);
         
        
          foreach($imagenes as $img) {
              if($j==0){
              
              echo '<div class ="contenedorImagen"> <img src="'.$imagenes[0]["url"].'"></div>';
              $j++;
              $n++;
              }
              echo '<div class="imagen"> <img src="'.$img["url"].'" pertenece="'.$n.'"></div>';
          }

            
            echo '<br>';
            echo '<div class="contenidoProduc">';
            foreach ($contenido as $conte) {
                $i++;
                if ($i <= $index) {

                    if ($conte != "imagenes" && $conte != "nombreProducto") {
                        echo $conte . ': ' . $producto[$conte] . '<br>';
                    } elseif ($conte == "nombreProducto") {
                        echo 'Nombre Producto: ' . $producto["nombre"] . '<br>';
                    }
                }

                //foreach producto
            }
            $i = 0;
            $j = 0;
            echo "Precio: " . $producto["precioVenta"] . "</br>";
            echo "Existencias: ".$producto["cantidad"]."</br>";
            echo "Cantidad a comprar: " . "<input type='number' class='cantidadCarro' name='cantidad' min='1' value='1'>" . "</br>";
            echo "<input class='agregarCarro' type='submit' producto='" . $producto["id"] . "' pertenece='" . $k . "' value='agregar'>";
            echo "<input class='suscribirProducto' type='submit' producto='" . $producto["id"] . "' pertenece='" . $k . "' value='suscribir'>";
            echo '</div>';
            echo '</div>';
            $k++;

            //primer foreach
        }
        $k = 0;
    }

    public function agregarCarrito() {
        //echo $_POST["cantidad"];
        //echo $_POST["producto"];
        $producto = Producto::search("id =" . $_POST["producto"]);
        $bodega = Productos_x_bodega::search("id_producto =" . $_POST["producto"]);

        $user = Usuario::search("id =" . $_SESSION["ID"]);
        $i = 0;
        $j = 0;
        $total = 0;
        $numero = count($bodega);
        $cantidadProducto = $_POST["cantidad"];
        $resta = 0;

        foreach ($bodega as $bode) {
            if ($_POST["cantidad"] <= $bode["cantidad"]) {
                $queda["cantidad"] = $bode["cantidad"] - $_POST["cantidad"];

                $producto = Producto::getBy("id", $_POST["producto"]);
                $bodega = Bodega::getBy("id", $bode["id_bodega"]);

                Model::deleteAccess("productos_x_bodega", "id_bodega =" . $bode["id_bodega"] . " and id_producto =" . $_POST["producto"]);
                $bodega->has_many("productos_x_bodega", $producto, $queda);
                $bodega->update();

                $data = array(
                    "nombre" => $user[0]["nombre"],
                    "apellido" => $user[0]["apellido"],
                    "cantidad" => $_POST["cantidad"],
                    "nombreProducto" => $producto->getNombre(),
                    "precio_unidad" => $producto->getPrecioVenta(),
                    "email" => $user[0]["email"]
                );
                //print_r($data);
                Factura::generar(100, $data);
                echo " Comprado  ";
                break;
            } else if ($_POST["cantidad"] > $bode["cantidad"]) {
                $total = $total + $bode["cantidad"];
            }
            $i++;

            if ($i == $numero) {
                if ($total < $_POST["cantidad"]) {
                    echo "no tenemos esa cantidad";
                } else {
                    foreach ($bodega as $bod) {

                        if ($bod["cantidad"] < $cantidadProducto) {
                            $valor["cantidad"] = 0;
                            //echo $bod["cantidad"]."  cantidad en bodega "."<br>";
                            //echo $cantidadProducto." CantidadProducto  "."<br>";

                            $resta = $bod["cantidad"] - $cantidadProducto;

                            if ($resta < 0) {
                                $resta = $resta * -1;
                            }
                            $j++;

                            $queda["cantidad"] = 0;
                            $producto = Producto::getBy("id", $_POST["producto"]);
                            $bodega = Bodega::getBy("id", $bod["id_bodega"]);

                            Model::deleteAccess("productos_x_bodega", "id_bodega =" . $bod["id_bodega"] . " and id_producto =" . $_POST["producto"]);
                            $bodega->has_many("productos_x_bodega", $producto, $queda);
                            $bodega->update();

                            if ($j != $numero) {
                                $cantidadProducto = $resta;
                                echo $cantidadProducto . " cantidad del producto actual    ";
                            } else {
                                echo $bod["cantidad"] - $cantidadProducto;
                            }
                        } else {
                            $valor2["cantidad"] = $bod["cantidad"] - $cantidadProducto;

                            $producto = Producto::getBy("id", $_POST["producto"]);
                            $bodega = Bodega::getBy("id", $bod["id_bodega"]);

                            Model::deleteAccess("productos_x_bodega", "id_bodega =" . $bod["id_bodega"] . " and id_producto =" . $_POST["producto"]);
                            $bodega->has_many("productos_x_bodega", $producto, $valor2);
                            $bodega->update();
                            $data = array(
                                "nombre" => $user[0]["nombre"],
                                "apellido" => $user[0]["apellido"],
                                "cantidad" => $_POST["cantidad"],
                                "nombreProducto" => $producto->getNombre(),
                                "precio_unidad" => $producto->getPrecioVenta(),
                                "email" => $user[0]["email"]
                            );
                            //print_r($data);
                            Factura::generar(100, $data);
                            echo " Comprado  ";
                            break;
                        }


                        //fianl foreach
                    }
                }
            }
        }

        //final carrito 
    }
    
    public function suscribirProducto(){
        
        $usuario=$_POST["usuario"];
        $producto=$_POST["producto"];
        $where="id_usuario=".$usuario." and id_producto=".$producto;
        $existe= Notificacion_producto::search($where);
        if($existe==NULL){
        $notficacion= new Notificacion_producto(null, $usuario, $producto);
        $notficacion->create();
        echo 1;
        }  else {
            echo -1;
        }
        
    }
    
    public function suscribirCategoria(){
        
        $usuario=$_POST["usuario"];
        $categoria=$_POST["categoria"];
        $where="id_usuario=".$usuario." and id_categoria=".$categoria;
        $existe= Notificacion_categoria::search($where);
        if($existe==NULL){
        $notficacion= new Notificacion_categoria(null, $usuario, $categoria);
        $notficacion->create();
        echo 1;
        }  else {
            echo -1;
        }
        
    }
    
    public function suscribirSubcategoria(){
        
        $usuario=$_POST["usuario"];
        $subcategoria=$_POST["subcategoria"];
        
        /*$categoria= Categoria::search("id =" .$subcategoria);
        $categoria=$categoria[0]["id_padre"];
        $whereCat="id_usuario=".$usuario." and id_categoria=".$subcategoriacategoria;
        $existeCat= Notificacion_categoria::search($whereCat);*/
        
        $whereSub="id_usuario=".$usuario." and id_categoria=".$subcategoria;
        $existeSub= Notificacion_categoria::search($whereSub);
        
        if($existeSub==NULL){
        $notficacion= new Notificacion_categoria(null, $usuario, $subcategoria);
        $notficacion->create();
        echo 1;
        }  else {
            echo -1;
        }
        
    }

}
