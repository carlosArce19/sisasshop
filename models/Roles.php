<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Roles
 *
 * @author Usuario
 */
class Roles extends Model{
    protected static $table="roles";
    private $id;
    private $nombre;
    
    function __construct($id, $nombre) {
        $this->id = $id;
        $this->nombre = $nombre;
    }

        
    public function getMyVars() {
        return get_object_vars($this);
    }
    
    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }


}
