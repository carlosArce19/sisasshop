<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario_controller
 *
 * @author pabhoz
 */
class Usuario_controller extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function userInfo($id) {
        $usr = Usuario::getById($id);
        $this->view->user = $usr;
        $this->view->render($this, "userInfo");
    }

    public function signIn() {
        
        
        if (isset($_POST["email"]) && isset($_POST["password"]) && isset($_POST["tienda"])) {


            $response = Usuario::where("email", $_POST["email"]);
            $response = $response[0];

            if ($response["password"] == $_POST["password"]) {

                if ($_POST["tienda"] != -1) {


                    Session::set('ID', $response["id"]);
                    Session::set('EMAIL', $response["email"]);
                    Session::set('NOMBRE', $response["nombre"]);
                    Session::set('APELLIDO', $response["apellido"]);
                    Session::set('ROL', $response["rol"]);
                    Session::set("TIENDA", $_POST["tienda"]);


                    echo '1';
                } else {
                    echo '-1';
                }
            } else {
                echo '-2';
            }
        }
    }

    public function signUp() {

        if (isset($_POST["nombre"]) && isset($_POST["apellido"]) && isset($_POST["password"]) && isset($_POST["email"])) {

            $data["nombre"] = $_POST["nombre"];
            $data["apellido"] = $_POST["apellido"];
            $data["password"] = $_POST["password"];
            $data["email"] = $_POST["email"];
            $data["rol"] = $_POST["rol"];




            $errorTienda = 0;
            $errorUsuario = 0;
            $errorBodega = 0;

            $usr = new Usuario(null, $data["nombre"], $data["apellido"], $data["password"], $data["email"], $data["rol"]);
            $r = $usr->create();
            print_r($r);
            if ($r["error"] == 1) {
                $errorUsuario = 1;
            }

            if (isset($_POST["nomTienda"]) && $_POST["nomTienda"] != NULL && $_POST["nomTienda"] != " ") {


                $template = new Template(null, "juansito", null, _URL . "/views/templates/" . "juansito");
                $template->create();

                $tienda = new Tienda(null, $_POST["nomTienda"], $usr->getId(), $template->getId());
                $t = $tienda->create();
                print_r($t);
                if ($t["error"] === 1) {
                    $errorTienda = 1;
                }

                $bodega = new Bodega(null, $_POST["nomTienda"] . " Bodega", $tienda->getId());
                $b = $bodega->create();
            }

            if ($errorTienda === 0 && $errorUsuario === 0 && $errorBodega === 0) {

                header("Location:" . _URL);
            } else {
                echo 'Error al crear el usuario';
            }
        }
    }

}
