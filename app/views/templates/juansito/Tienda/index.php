<!DOCTYPE html>

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <title>Sisas Shop Tienda</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script src="./libs/tienda.js"></script>
        <link rel="stylesheet" href="./views/templates/juansito/Panel/css/app.css">
    </head>
    <body>
        <div class="panelIzq"> 
            <br>
            <div class="usuario"> <?php echo $_SESSION["NOMBRE"]." ".$_SESSION["APELLIDO"];?> <br> <?php echo $_SESSION["EMAIL"];?> </div>
            <div class="barraH">

            </div>
        </div>

        <div class="workspace">
            

        </div>

        <script>
            $("body").on("click", ".suscribirProducto", function (e) {
                e.preventDefault();
                e.stopPropagation();
                
                var target = e.target;
                target = $(target).attr("producto");
                var usuario= <?php echo $_SESSION["ID"]; ?>;
                
                $.ajax({
                    type: "POST",
                    url: "<?php echo _URL; ?>/tienda/suscribirProducto",
                    data: {usuario: usuario, producto: target}
                }).done(function (response) {

                    if (response == 1) {
                        alert("Suscrito Exitosamente");
                    } else {
                        alert("Ya se encuentra suscrito a este producto");
                        //alert(response);
                    }
                });

            });
            $("body").on("click", ".suscribirCategoria", function (e) {
                e.preventDefault();
                e.stopPropagation();
                var target = e.target;
                target = $(target).attr("categoria");
                var usuario= <?php echo $_SESSION["ID"]; ?>;
                
                $.ajax({
                    type: "POST",
                    url: "<?php echo _URL; ?>/tienda/suscribirCategoria",
                    data: {usuario: usuario, categoria: target}
                }).done(function (response) {

                    if (response == 1) {
                        alert("Suscrito Exitosamente");
                    } else {
                        alert("Ya se encuentra suscrito a esta categoria");
                        //alert(response);
                    }
                });

            });
            $("body").on("click", ".suscribirSubcategoria", function (e) {
                e.preventDefault();
                e.stopPropagation();
                var target = e.target;
                target = $(target).attr("subcategoria");
                var usuario= <?php echo $_SESSION["ID"]; ?>;
                
               
                $.ajax({
                    type: "POST",
                    url: "<?php echo _URL; ?>/tienda/suscribirSubcategoria",
                    data: {usuario: usuario, subcategoria: target}
                }).done(function (response) {

                    if (response == 1) {
                        alert("Suscrito Exitosamente");
                    } else {
                        alert("Ya se encuentra suscrito a esta categoria");
                        //alert(response);
                    }
                });

            });
        </script>
    </body>
</html>

