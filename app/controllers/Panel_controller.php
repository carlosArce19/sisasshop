<?php

class Panel_controller extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $cat = Categoria::search("id_padre is null");
        $this->view->categorias = $cat;

        $tienda = Tienda::search("owner =" . $_SESSION["ID"]);
        $tiendaTemplate = $tienda[0]["id_template"];
        $tiendaId = $tienda[0]["id"];

        $template = Template::search("id =" . $tiendaTemplate);
        $template = $template[0]["nombre"];

        $sub = Categoria::search("id_padre is not null");
        $this->view->subcategorias = $sub;

        $this->view->tienda = $tienda;


        $pro = Producto::getAll();
        $this->view->pro = $pro;

        $bodeg = Bodega::search("id_tienda =" . $tiendaId);
        $this->view->bode = $bodeg;

        $roles = Roles::getAll();
        $this->view->rol = $roles;

        $templates = Template::getAll();
        $this->view->template = $templates;



        $this->view->render($this, $template, "index");
    }

    public function categoria() {
        $categoria = new Categoria(null, $_POST["nombre"], null);
        $categoria->create();
        $total = "";

        if (isset($_POST["nombreProducto"])) {
            $total = $total . $_POST["nombreProducto"];
        }
        if (isset($_POST["titulo"])) {
            $total = $total . $_POST["titulo"];
        }
        if (isset($_POST["descripcion"])) {
            $total = $total . $_POST["descripcion"];
        }
        if (isset($_POST["imagenes"])) {
            $total = $total . $_POST["imagenes"];
        }
        if (isset($_POST["fechaVencimiento"])) {
            $total = $total . $_POST["fechaVencimiento"];
        }
        if (isset($_POST["keywords"])) {
            $total = $total . $_POST["keywords"];
        }
        if (isset($_POST["talla"])) {
            $total = $total . $_POST["talla"];
        }
        if (isset($_POST["color"])) {
            $total = $total . $_POST["color"];
        }
        if (isset($_POST["editorial"])) {
            $total = $total . $_POST["editorial"];
        }
        if (isset($_POST["fabricante"])) {
            $total = $total . $_POST["fabricante"];
        }
        if (isset($_POST["autor"])) {
            $total = $total . $_POST["autor"];
        }
        if (isset($_POST["peso"])) {
            $total = $total . $_POST["peso"];
        }

        $contenido = new Contenido(null, $total, null);
        $contenido->create();
        $contenido->has_one("contenido", $categoria);
        $contenido->update();
        header("Location:" . _URL . "/panel");
    }

    public function subcategoria() {
        $categoria = new Categoria(null, $_POST["nombre"], null);
        $categoria->create();

        $categoria = Categoria::getBy("nombre", $_POST["nombre"]);
        $categoriaPadre = Categoria::getById($_POST["id_padre"]);

        $categoria->has_one("categoria", $categoriaPadre);
        $categoria->update();
        header("Location:" . _URL . "/panel");
    }

    public function template() {

        //TODO: verificar que sea un ZIP o ZipArchive:open() dará error.
        $archivo = $_FILES["file"]["name"];
        $ext = pathinfo($archivo, PATHINFO_EXTENSION);
        if ($ext == "zip") {
            $archivo = substr($archivo, 0, -4);
            $file = File::upload("./views/templates/", "file");

            $ruta = _URL . "/views/templates/" . $archivo;
            $zip = new ZipArchive;
            $res = $zip->open($file);
            if ($res === TRUE) {
                $zip->extractTo('./views/templates/');
                $zip->close();
                unlink($file);
                $tpl = new Template(null, $archivo, null, $ruta);
                $tpl->create();
                //TODO TRUE
            } else {
                //TODO FALSE
            }
        } else {
            echo "El archivo que agrego no es un .zip";
        }
        header("Location:" . _URL . "/panel");
    }

    public function crearProducto() {

        if (!isset($_POST["nombreProducto"])) {
            $_POST["nombreProducto"] = " ";
        }
        if (!isset($_POST["titulo"])) {
            $_POST["titulo"] = null;
        }
        if (!isset($_POST["descripcion"])) {
            $_POST["descripcion"] = $total . $_POST["descripcion"];
        }
        if (!isset($_POST["files[]"])) {
            $_POST["files[]"] = null;
        }
        if (!isset($_POST["fechaVencimiento"])) {
            $_POST["fechaVencimiento"] = null;
        }
        if (!isset($_POST["keywords"])) {
            $_POST["keywords"] = null;
        }
        if (!isset($_POST["talla"])) {
            $_POST["talla"] = null;
        }
        if (!isset($_POST["color"])) {
            $_POST["color"] = null;
        }
        if (!isset($_POST["editorial"])) {
            $_POST["editorial"] = null;
        }
        if (!isset($_POST["fabricante"])) {
            $_POST["fabricante"] = null;
        }
        if (!isset($_POST["autor"])) {
            $_POST["autor"] = null;
        }
        if (!isset($_POST["peso"])) {
            $_POST["peso"] = null;
        }

        $producto = new Producto(null, $_POST["nombreProducto"], $_POST["subcategoria"], $_POST["titulo"], $_POST["descripcion"], $_POST["precioVenta"], $_POST["precioCompra"], $_POST["codigo"], $_POST["codigoBarras"], $_POST["cantidad"], $_POST["fechaVencimiento"], $_POST["keywords"], $_POST["talla"], $_POST["color"], $_POST["editorial"], $_POST["fabricante"], $_POST["autor"], $_POST["peso"]);
        $producto->create();


        $tienda = Tienda::search("owner =" . $_SESSION["ID"]);
        $tiendaId = $tienda[0]["id"];

        $bodega = Bodega::getBy("id_tienda", $tiendaId);
        $cantidad['cantidad'] = $_POST["cantidad"];


        $bodega->has_many("productos_x_bodega", $producto, $cantidad);
        $bodega->update();

        //$bodega->has_many("productos_x_bodega", $producto,$cantidad);
        //$bodega->update();



        $file_ary = array();
        $file_count = count($_FILES['files']['name']);
        $file_keys = array_keys($_FILES['files']);

        for ($i = 0; $i < $file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $_FILES['files'][$key][$i];
            }
        }
        //comprobar sino esta vacio 
        if ($_FILES['files']['error'][0] != "4") {
            foreach ($file_ary as $imagenes) {
                $ruta = _URL . "/public/productos/" . $imagenes['name'];
                $imagen = new Imagen(null, $ruta, null);
                $imagen->create();
                $imagen->has_one("producto", $producto);
                $imagen->update();
                move_uploaded_file($imagenes["tmp_name"], "./public/productos/" . basename($imagenes["name"]));
            }
        }



        self::enviarCorreosSubsCategoria($producto->getId_categoria(), $producto->getNombre());
        self::escribirLog("Creacion Producto", $_POST["nombreProducto"], $_SESSION["EMAIL"]);
        header("Location:" . _URL . "/panel");
    }

    public function crearBodega() {
        $bodega = new Bodega(null, $_POST["nombre"], $_POST["id_tienda"]);
        $bodega->create();

        if (isset($_SESSION["EMAIL"])) {
            $responsable = $_SESSION["EMAIL"];
            self::escribirLog("Creacion Bodega", $_POST["nombre"], $responsable);
            header("Location:" . _URL . "/panel");
        } else {

            $responsable = "Sistema";
            self::escribirLog("Creacion Bodega", $_POST["nombre"], $responsable);
        }
    }

    public function añadirBodega() {
        $añadir = Productos_x_bodega::search("id_producto =" . $_POST["id_producto"] . " and id_bodega =" . $_POST["id_bodega"]);
        $bodega = Bodega::getBy("id", $_POST["id_bodega"]);
        $producto = Producto::getBy("id", $_POST["id_producto"]);
        $pro = Producto::where("id", $_POST["id_producto"]);
        $bode = Bodega::where("id", $_POST["id_bodega"]);

        if (count($añadir) > 0) {
            //habia antes y agrego mas   
            Model::deleteAccess("productos_x_bodega", "id_bodega =" . $_POST["id_bodega"] . " and id_producto =" . $_POST["id_producto"]);
            $cantidadExistente["cantidad"] = $añadir[0]["cantidad"] + $_POST["cantidad"];

            $bodega->has_many("productos_x_bodega", $producto, $cantidadExistente);
            $bodega->update();
        } else {

            //el producto es nuevo
            $cantidadExistente["cantidad"] = $_POST["cantidad"];
            $bodega->has_many("productos_x_bodega", $producto, $cantidadExistente);
            $bodega->update();
        }

        self::enviarCorreosSubsProducto($pro[0]["id"], $pro[0]["nombre"]);
        self::enviarCorreosSubsCategoria($pro[0]["id_categoria"], $pro[0]["nombre"]);
        self::escribirLog("Adicion Producto" . $pro[0]["nombre"], "Canidad en bodega " . $bode["nombre"], $_SESSION["EMAIL"]);
        header("Location:" . _URL . "/panel");
    }

    public function enviarCorreos() {

        if (isset($_POST["asunto"]) && isset($_POST["mensaje"]) && isset($_POST["destinatario"]) && isset($_POST["tienda"]) && $_POST["destinatario"] != -1 && $_POST["tienda"] != "Seleccione la tienda") {

            $asunto = $_POST["asunto"];
            $mensaje = $_POST["mensaje"];
            $destinatario = $_POST["destinatario"];
            $tienda = $_POST["tienda"];
            $emisor = $_SESSION["EMAIL"];
            $usuarios = $destinatario == -2 ? Usuario::getAll() : Usuario::where("rol", $destinatario);

            foreach ($usuarios as $usr) {
                $cuerpo = "Buenas " . $usr["nombre"] . " " . $usr["apellido"] . " de parte de " . $tienda . "\n \n" . $mensaje;
                //mail($usr["email"], $asunto, $cuerpo, $emisor);
                $msn = new AttachMailer($emisor, $usr["email"], $asunto, $cuerpo);
                $msn->send();
            }
            echo '1';
        } else {
            echo 'Profavor llene todos los campos.';
        }
    }

    public static function enviarCorreosSubsCategoria($idCategoria, $nombreProducto) {

        $noticacion = Notificacion_categoria::where("id_categoria", $idCategoria);
        $emisor = $_SESSION["EMAIL"];
        foreach ($noticacion as $noti) {


            $idUsr = $noti["id_usuario"];
            $cliente = Usuario::where("id", $idUsr);
            $emailcliente = $cliente[0]["email"];
            $cuerpo = "buenas " . $cliente[0]["nombre"] . " " . $cliente[0]["apellido"] . " nos complace informale que se agrego el nuevo producto " . $nombreProducto . " que creemos le va ha interesar";
            $msn = new AttachMailer($emisor, $emailcliente, "Nuevo Producto", $cuerpo);
            $msn->send();
        }
    }

    public static function enviarCorreosSubsProducto($idProducto, $nombreProducto) {

        $noticacion = Notificacion_producto::where("id_producto", $idProducto);
        $emisor = $_SESSION["EMAIL"];
        foreach ($noticacion as $noti) {


            $idUsr = $noti["id_usuario"];
            $cliente = Usuario::where("id", $idUsr);
            $emailcliente = $cliente[0]["email"];
            $cuerpo = "buenas " . $cliente[0]["nombre"] . " " . $cliente[0]["apellido"] . " nos complace informale que se agrego el nueva mercancia al producto " . $nombreProducto . " que creemos le va ha interesar";
            $msn = new AttachMailer($emisor, $emailcliente, "Nuevo Stock de Producto", $cuerpo);
            $msn->send();
        }
    }

    public static function escribirLog($tipocambio, $cambio, $responsable) {

        $archivo = fopen("D:/xampp/htdocs/sisa_Shop/app". "/public/log/log.txt", "a");
        $hoy = getdate();
        $dia = $hoy["mday"];
        $mes = $hoy["mon"];
        $ano = $hoy["year"];

        $hora = $hoy["hours"];
        $min = $hoy["minutes"];

        $texto = "Tipo de Cambio: " . $tipocambio . " || Cambio: " . $cambio . " || Responsable: " . $responsable . " || Fecha: " . $dia . "/" . $mes . "/" . $ano . " || hora: " . $hora . ":" . $min . "\n";
        print_r($texto);
        fwrite($archivo, $texto);
        fclose($archivo);
    }

    public function cambiarTemplate() {

        $template = $_POST["id_template"];
        $id_tienda = $_POST["id_tienda"];
        $tienda = Tienda::getById($id_tienda);
        $tienda->setId_template($template);
        $tienda->update();
        header("Location:" . _URL . "/panel");
    }

    public function editarProducto() {

        $producto = Producto::getById($_POST["productoEditar"]);
        $cambios = "[" . $producto->getId() . "]";
        if (!isset($_POST["eliminar"])) {
            echo 'entro 1';
            if (isset($_POST["nombreProducto"])) {
                if ($_POST["nombreProducto"] != "") {
                    $producto->setNombre($_POST["nombreProducto"]);
                    $cambios+="[nombreProducto] ";
                }
            }

            if (isset($_POST["titulo"])) {
                if ($_POST["titulo"] != "") {
                    $producto->setTitulo($_POST["titulo"]);
                    $cambios+="[titulo] ";
                }
            }

            if (isset($_POST["descripcion"])) {
                if ($_POST["descripcion"] != "") {
                    $producto->setTitulo($_POST["descripcion"]);
                    $cambios+="[descripcion] ";
                }
            }

            if (isset($_POST["precioVenta"])) {
                if ($_POST["precioVenta"] != "") {
                    $producto->setPrecioVenta($_POST["precioVenta"]);
                    $cambios+="[precioVenta] ";
                }
            }

            if (isset($_POST["precioCompra"])) {
                if ($_POST["precioCompra"] != "") {
                    $producto->setPrecioCompra($_POST["precioCompra"]);
                    $cambios+="[precioCompra] ";
                }
            }

            if (isset($_POST["codigo"])) {
                if ($_POST["codigo"] != "") {
                    $producto->setCodigo($_POST["codigo"]);
                    $cambios+="[codigo] ";
                }
            }

            if (isset($_POST["codigoBarras"])) {
                if ($_POST["codigoBarras"] != "") {
                    $producto->setCodigoBarras($_POST["codigoBarras"]);
                    $cambios+="[codigoBarras] ";
                }
            }

            if (isset($_POST["imagenes"])) {
                if ($_POST["imagenes"] != -1) {

                    $imagen = Imagen::getById($_POST["imagenes"]);
                    self::escribirLog("Eliminar imagen", $imagen->getId(), $_SESSION["EMAIL"]);
                    $imagen->delete();
                }
            }

            if (isset($_POST["fechaVencimiento"])) {
                if ($_POST["fechaVencimiento"] != "") {
                    $producto->setFechaVencimiento($_POST["fechaVencimiento"]);
                    $cambios+="[fechaVencimiento] ";
                }
            }

            if (isset($_POST["keywords"])) {
                if ($_POST["keywords"] != "") {
                    $producto->setKeywords($_POST["keywords"]);
                    $cambios+="[keywords] ";
                }
            }

            if (isset($_POST["talla"])) {
                if ($_POST["talla"] != "") {
                    $producto->setTalla($_POST["talla"]);
                    $cambios+="[talla] ";
                }
            }

            if (isset($_POST["color"])) {
                if ($_POST["color"] != "") {
                    $producto->setColor($_POST["color"]);
                    $cambios+="[color] ";
                }
            }

            if (isset($_POST["editorial"])) {
                if ($_POST["editorial"] != "") {
                    $producto->setEditorial($_POST["editorial"]);
                    $cambios+="[editorial] ";
                }
            }

            if (isset($_POST["fabricante"])) {
                if ($_POST["fabricante"] != "") {
                    $producto->setFabricante($_POST["fabricante"]);
                    $cambios+="[fabricante] ";
                }
            }

            if (isset($_POST["autor"])) {
                if ($_POST["autor"] != "") {
                    $producto->setAutor($_POST["autor"]);
                    $cambios+="[autor] ";
                }
            }

            if (isset($_POST["peso"])) {
                if ($_POST["peso"] != "") {
                    $producto->setPeso($_POST["peso"]);
                    $cambios+="[peso] ";
                }
            }

            $producto->update();

            $file_ary = array();
            $file_count = count($_FILES['files']['name']);
            $file_keys = array_keys($_FILES['files']);

            for ($i = 0; $i < $file_count; $i++) {
                foreach ($file_keys as $key) {
                    $file_ary[$i][$key] = $_FILES['files'][$key][$i];
                }
            }
            //comprobar sino esta vacio 
            if ($_FILES['files']['error'][0] != "4") {
                foreach ($file_ary as $imagenes) {
                    $ruta = _URL . "/public/productos/" . $imagenes['name'];
                    $imagen = new Imagen(null, $ruta, null);
                    $imagen->create();
                    $imagen->has_one("producto", $producto);
                    $imagen->update();
                    move_uploaded_file($imagenes["tmp_name"], "./public/productos/" . basename($imagenes["name"]));
                    $cambios+="[Imagen Adicionda] ";
                }
            }

            self::escribirLog("Edicion Producto", $cambios, $_SESSION["EMAIL"]);
            header("Location:" . _URL . "/panel");
        } else {


            $imagen = Imagen::where("id_producto", $producto->getId());

            foreach ($imagen as $img) {
                $id = $img["id"];
                $imgEliminar = Imagen::getById($id);
                self::escribirLog("Eliminar imagen", $imgEliminar->getId(), $_SESSION["EMAIL"]);
                $imgEliminar->delete();
            }
            $bodega = Productos_x_bodega::where("id_producto", $producto->getId());

            foreach ($bodega as $bod) {

                $idBod = $bod["id_producto"];
                $bodEliminar = Productos_x_bodega::getBy("id_producto", $idBod);
                self::escribirLog("Eliminar Productos_x_bodega", "Bod[" . $bodEliminar->getId_bodega() . "]Pro[" . $idBod . "]", $_SESSION["EMAIL"]);
                Model::deleteAccess("productos_x_bodega", "id_bodega =" . $bodEliminar->getId_bodega() . " and id_producto =" . $idBod);
            }

            self::escribirLog("Eliminar Producto", "[" . $producto->getId() . "]", $_SESSION["EMAIL"]);
            $producto->delete();
            header("Location:" . _URL . "/panel");
        }
       
    }

    public function compruebaDatos() {
        Sisa_BL::categoriaDatos($_POST["id"]);
    }

    public function traerDatosProducto() {
        Sisa_BL::productosDatos($_POST["id_producto"]);
    }

}