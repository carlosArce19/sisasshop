<?php

/**
 * Description of Usuario
 *
 * @author pabhoz
 */
class Usuario extends Model {

    protected static $table = "Usuario";
    private $id;
    private $nombre;
    private $apellido;
    private $password;
    private $email;
    private $rol;
    
       
    function __construct($id, $nombre, $apellido, $password, $email, $rol) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->password = $password;
        $this->email = $email;
        $this->rol = $rol;
    }

    
    public function getMyVars() {
        return get_object_vars($this);
    }
    
    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getApellido() {
        return $this->apellido;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getRol() {
        return $this->rol;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setApellido($apellido) {
        $this->apellido = $apellido;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setRol($rol) {
        $this->rol = $rol;
    }


}
