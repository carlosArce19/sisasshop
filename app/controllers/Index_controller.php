<?php

  class Index_controller extends Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    
    function index(){
        $tienda=  Tienda::getAll();
        $this->view->tienda = $tienda;

        $this->view->render($this,"juansito","index");
    }
  }