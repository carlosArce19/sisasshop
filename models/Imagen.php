<?php

class Imagen extends Model{
    protected static $table= "imagen";
    private $id;
    private $url;
    private $id_producto;
    
        private $has_one = array(
        'producto' => array(
            'class' => 'Producto',
            'join_as' => 'id_producto',
            'join_with' => 'id'
        )
    );
    
    function __construct($id, $url, $id_producto) {
        $this->id = $id;
        $this->url = $url;
        $this->id_producto = $id_producto;
    }
       public function getMyVars() {
        return get_object_vars($this);
    }
    
    public function getId() {
        return $this->id;
    }

    public function getUrl() {
        return $this->url;
    }

    public function getId_producto() {
        return $this->id_producto;
    }

    public function getHas_one() {
        return $this->has_one;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function setId_producto($id_producto) {
        $this->id_producto = $id_producto;
    }

    public function setHas_one($has_one) {
        $this->has_one = $has_one;
    }



}